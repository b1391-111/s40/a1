console.log("DOM Manipulation");

// Finding HTML Elements
    // document.getElementById();
    // document.getElementsByTagName()
    // document.getElementsByClassName();

    // document.querySelector('#id');
    // document.querySelector('tag');
    // document.querySelector('.classname');

// Change HTML Elements
    // Property
        // element.innerHTML = new html content
        // const myTitle = document.querySelector('h1');
        // myTitle.innerHTML = `Hello World`;
        // myTitle.className = 'title-1';
        // myTitle.id = 'title-1';
        // console.log(myTitle);

    // Event Listener
    const firstName = document.querySelector('#firstN');
    const lastName = document.querySelector('#lastN');
    const fullName = document.querySelector('#fullN');

    let inputFN = '';
    let inputLN = '';
    
    firstName.addEventListener("keyup", (e) => {
        inputFN = e.target.value;
        fullName.innerHTML = `${inputFN} ${inputLN}`
    });
    
    lastName.addEventListener("keyup", (e) => {
        inputLN = e.target.value;
        fullName.innerHTML = `${inputFN} ${inputLN}`
    });